#!/bin/sh
set -e
flask db upgrade
gunicorn --log-file=- --workers=${WORKERS} --threads=${THREADS} --timeout=300 run:flask_app -b 0.0.0.0:5000 -c gunicorn_config.py