import json
import logging
from http import HTTPStatus

from flask import g, request

from app import create_app
from app.cache import flask_app_cache
from app.lib.constants import DEFAULT_ERROR_MESSAGE
from app.lib.cryptor import FlaskAppCryptor
from app.repository.auth_token_repo import AuthTokenRepo
from flask import jsonify

request_logger = logging.getLogger('RequestLogger')

config_env = 'config.DevelopmentConfig'

flask_app = create_app(config_env)


class RequestPayloadProcessingError(Exception):
    """
    Raise when an request payload is not processed properly.
    """
    def __init__(self, msg=DEFAULT_ERROR_MESSAGE, *args, **kwargs):
        super().__init__(msg, *args, **kwargs)


def is_ignored_path(path):
    ignored_paths = [
        '/ping',
        '/downgrade_db',
        '/seed_db',
        '/swagger',
        '/api/login_token',
        '/api/signup_token',
        '/api/logout',
        '/api/user/deactivate',
        '/api/user/password_reset',
        '/api/user/validate_verification_code',
        '/api/user/set_new_password'
    ]
    return path in ignored_paths


@flask_app.before_request
def app_before_request():
    swagger_paths = ['/swagger', '/flasgger', '/apispec', '/static', '/favicon']
    if any(request.path.startswith(p) for p in swagger_paths):
        return

    log_params = {
        'ip': request.headers.get("X-Forwarded-For", request.remote_addr),
        'method': request.method,
        'path': request.path,
    }
    if request.headers.get('X-Request-ID'):
        log_params['request_id'] = request.headers.get('X-Request-ID')

    if request.args:
        log_params['params'] = dict(request.args)

    request_logger.debug(json.dumps(log_params))

    if request.method in ['GET', 'OPTIONS']:
        return

    request.json_data = request.json
    if not request.json:
        return

    if request.headers.get('encrypt_payload') and flask_app.config.get('FLASK_ENV', 'prod') == 'dev':
        try:
            if bool(int(request.headers['encrypt_payload'])) is False:
                request_logger.debug(json.dumps({'path': request.path, 'request_json': request.json}))
                return
        except ValueError:
            pass

    if is_ignored_path(request.path):
        return

    try:
        encrypted_json = request.json.get('payload')
        if not encrypted_json:
            raise RequestPayloadProcessingError

        scheme, token_hash = request.headers['Authorization'].split(None, 1)
        token = flask_app_cache.get(token_hash)
        if token:
            aes_key = token['aes_key']
            aes_iv = token['aes_iv']
        else:
            token = AuthTokenRepo.get_auth_token_by_hash(token_hash)
            aes_key = token.aes_key
            aes_iv = token.aes_iv

        request.json_data = FlaskAppCryptor.decrypt(encrypted_json, aes_key, aes_iv)
        request_logger.debug(json.dumps({'path': request.path, 'request_json': request.json_data}))
    except RequestPayloadProcessingError as ex:
        return jsonify('No payload provided. Unable to process the request.'), HTTPStatus.BAD_REQUEST
    except Exception as ex:
        return jsonify('Invalid token or token expired. You are not authorised.'), HTTPStatus.UNAUTHORIZED


@flask_app.after_request
def app_after_request(response):
    response.headers["X-Frame-Options"] = "SAMEORIGIN"

    swagger_paths = ['/swagger', '/flasgger', '/apispec', '/static', '/favicon']
    if any(request.path.startswith(p) for p in swagger_paths):
        return response

    if request.path == '/favicon.ico' or request.path.startswith('/static'):
        return response

    if request.headers.get('encrypt_payload') and flask_app.config.get('FLASK_ENV', 'prod') == 'dev':
        try:
            if bool(int(request.headers['encrypt_payload'])) is False:
                return response
        except ValueError:
            pass

    if is_ignored_path(request.path):
        return response

    try:
        response_data = response.get_data()
        if not response_data:
            return response

        scheme, token_hash = request.headers['Authorization'].split(None, 1)
        token = flask_app_cache.get(token_hash)
        if token:
            aes_key = token['aes_key']
            aes_iv = token['aes_iv']
        else:
            token = g.current_user.auth_token
            aes_key = token.aes_key
            aes_iv = token.aes_iv

        response_data = FlaskAppCryptor.encrypt(response_data.decode(), aes_key, aes_iv)
        response.set_data(json.dumps({'payload': response_data}))
    except Exception as ex:
        response.set_data('Invalid token or token expired. You are not authorised. Cannot encrypt the response.')
        response.status_code = HTTPStatus.UNAUTHORIZED

    return response


if __name__ == '__main__':
    flask_app.run()


@flask_app.shell_context_processor
def make_shell_context():
    return {
        'app': flask_app
    }
