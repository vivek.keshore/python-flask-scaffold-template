"""Flask config class."""
import json
import os

from dotenv import load_dotenv

load_dotenv()


def get_database_connection_url():
    secure_params = os.environ.get("DB_CONNECTION")

    if secure_params:
        params = json.loads(secure_params)
        params['database'] = os.environ.get('DB_NAME', 'my_app_db')
    else:
        params = {
            "username": os.environ.get('DB_USERNAME', ''),
            "password": os.environ.get('DB_PASSWORD', ''),
            "host": os.environ.get('DB_HOST', ''),
            "port": os.environ.get('DB_PORT', ''),
            "database": os.environ.get('DB_NAME', 'my_app_db'),
            "engine": 'postgres'
        }
    return '{engine}://{username}:{password}@{host}:{port}/{database}'.format(**params)


class BaseConfig:
    """Set Flask configuration vars."""
    SQLALCHEMY_DATABASE_URI = get_database_connection_url()
    SQLALCHEMY_TRACK_MODIFICATIONS = False if os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS', 'False') else True

    # Mail
    mail_params = {}
    if os.environ.get("SMTP_CONNECTION"):
        mail_params = json.loads(os.environ.get("SMTP_CONNECTION"))

    MAIL_SERVER = mail_params['host'] if mail_params else os.environ.get('MAIL_SERVER')
    MAIL_PORT = mail_params['port'] if mail_params else os.environ.get('MAIL_PORT')
    MAIL_USERNAME = mail_params['username'] if mail_params else os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = mail_params['password'] if mail_params else os.environ.get('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = mail_params['sender'] if mail_params else os.environ.get('MAIL_DEFAULT_SENDER')
    MAIL_USE_SSL = True if (mail_params['use_ssl'] if mail_params else os.environ.get('MAIL_USE_SSL')) == 'True' else False
    MAIL_USE_TLS = True if (mail_params['use_tls'] if mail_params else os.environ.get('MAIL_USE_TLS')) == 'True' else False

    CELERY_RESULT_BACKEND = 'redis://{host}:{port}/2'.format(
        host=os.environ.get('REDIS_HOST'),
        port=os.environ.get('REDIS_PORT')
    )

    CELERY_BROKER_URL = 'redis://{host}:{port}/1'.format(
        host=os.environ.get('REDIS_HOST'),
        port=os.environ.get('REDIS_PORT')
    )

    REDIS_CACHE_URL = 'redis://{host}:{port}/0'.format(
        host=os.environ.get('REDIS_HOST'),
        port=os.environ.get('REDIS_PORT')
    )

    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'

    DEVELOPER_EMAIL = os.environ.get('DEVELOPER_EMAIL')

    APPLICATION_PATH = os.getcwd()

    WEB_URI = '{protocol}://{host}:{port}'.format(
        protocol=os.environ.get('WEB_URI_PROTOCOL'),
        host=os.environ.get('WEB_URI_HOST'),
        port=os.environ.get('WEB_URI_PORT')
    )

    WEB_API = '{protocol}://{host}:{port}'.format(
        protocol=os.environ.get('WEB_API_PROTOCOL'),
        host=os.environ.get('WEB_API_HOST'),
        port=os.environ.get('WEB_API_PORT')
    )

    FLASK_ENV = os.environ.get('FLASK_ENV')
    FLASK_APP = os.environ.get('FLASK_APP')

    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    FLASK_DEBUG = False if os.environ.get('FLASK_DEBUG', 'False') == 'False' else True

    AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
    AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
    S3_BUCKET = os.environ.get('S3_BUCKET')


class DevelopmentConfig(BaseConfig):
    DEBUG = True

    # Logs
    LOG_LEVEL = 'DEBUG'
    LOG_TYPE = 'file'


class TestConfig(BaseConfig):
    TESTING = True
    DEBUG = False

    # Logs
    LOG_LEVEL = 'INFO'
    LOG_TYPE = 'file'
