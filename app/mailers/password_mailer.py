from celery.utils.log import get_task_logger
from flask import render_template
from flask_mail import Message

from app.mailers import Mailer
from app.mailers import mail

logger = get_task_logger(__name__)


class PasswordMailer(Mailer):
    def reset_password_mail(self, to_email, password_reset_token):
        logger.info(f'Trying to send reset password email to {to_email}')
        subject = f'{self.app_name} - Reset your password'
        msg = Message(
            f'{self.sub_prefix}{subject}',
            sender=self.sender_email,
            recipients=self.override_recipient_email(to_email)
        )
        msg.html = render_template('user_emails/reset_password_email.html',
                                   password_reset_token=password_reset_token)

        logger.info(f'Sending email. Message - {msg.html}')
        mail.send(msg)
        logger.info(f'Reset password email successfully sent to {to_email}')

    def reset_password_success_email(self, to_email, first_name):
        logger.info(f'Trying to send reset password success email to {to_email}')
        subject = f'{self.app_name} - Password reset successful'
        msg = Message(
            f'{self.sub_prefix}{subject}',
            sender=self.sender_email,
            recipients=self.override_recipient_email(to_email)
        )
        msg.html = render_template(
            'user_emails/reset_password_success_email.html',
            first_name=first_name,
            email=to_email
        )
        self.msg = msg
        logger.info(f'Sending email. Message - {msg.html}')
        self.email.send(msg)
        logger.info(f'Reset password successful email sent successfully to {to_email}')
