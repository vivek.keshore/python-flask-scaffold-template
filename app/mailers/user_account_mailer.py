from celery.utils.log import get_task_logger
from flask import render_template
from flask_mail import Message

from app.mailers import Mailer

logger = get_task_logger(__name__)


class UserAccountMailer(Mailer):

    def new_user_email(self, first_name, last_name, to_email):
        logger.info(f'Trying to new user registration success email to {to_email}')
        subject = f'{self.app_name} - Account created successfully'
        msg = Message(
            f'{self.sub_prefix}{subject}',
            sender=self.sender_email,
            recipients=self.override_recipient_email(to_email)
        )
        msg.html = render_template(
            'user_emails/new_user_account.html',
            first_name=first_name,
            last_name=last_name,
            email= to_email
        )
        self.msg = msg
        logger.info(f'Sending email. Message - {msg.html}')
        self.email.send(msg)
        logger.info(f'New user registration success email sent successfully to {to_email}')

    def update_user_email(self, to_email, first_name):
        logger.info(f'Trying to email related to user email address change to {to_email}')
        subject = f'{self.app_name} - Account updated successfully'
        msg = Message(
            f'{self.sub_prefix}{subject}',
            sender=self.sender_email,
            recipients=self.override_recipient_email(to_email)
        )
        msg.html = render_template(
            'user_emails/update_user_account.html',
            first_name=first_name,
            email=to_email
        )
        self.msg = msg
        logger.info(f'Sending email. Message - {msg.html}')
        self.email.send(msg)
        logger.info(f'User email changed. Notification email sent successfully to {to_email}')

    def send_login_otp(self, otp, to_email, first_name):
        logger.info(f'Trying to send one time verification code email to {to_email}')
        subject = f'{self.app_name} - verification code for login'
        msg = Message(
            f'{self.sub_prefix}{subject}',
            sender=self.sender_email,
            recipients=self.override_recipient_email(to_email)
        )
        msg.html = render_template(
            'user_emails/otp_template.html',
            first_name=first_name,
            otp=otp,
            email=to_email
        )
        self.msg = msg
        logger.info(f'Sending email. Message - {msg.html}')
        self.email.send(msg)
        logger.info(f'One time verification code email sent successfully to {to_email}')
