from flask import current_app as flask_app
from flask_mail import Mail

from app.lib.utils import to_list

mail = Mail()


class Mailer(object):

    def __init__(self):
        self.email = mail
        self.msg = None
        self.env = flask_app.config.get('ENV', 'dev')
        self.sub_prefix = f'{self.env} - ' if self.env != 'prod' else ''
        self.sender_email = flask_app.config.get('MAIL_DEFAULT_SENDER')
        self.app_name = flask_app.config.get('APP_NAME', 'Flask App')

    @staticmethod
    def override_recipient_email(email_recipients):
        if flask_app.config.get('DEVELOPER_EMAIL') is not None:
            email_recipients = flask_app.config.get('DEVELOPER_EMAIL').split(',')
        return to_list(email_recipients)
