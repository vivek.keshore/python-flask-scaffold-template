from celery.utils.log import get_task_logger

from app.mailers.password_mailer import PasswordMailer
from app.mailers.user_account_mailer import UserAccountMailer
from app.tasks.base_task import flask_app_celery

logger = get_task_logger(__name__)


@flask_app_celery.task(bind=True, queue='new_user_email')
def new_user_email(unused_arg, first_name, last_name, email):
    try:
        logger.info(f'Sending account creation successful email to {email}')
        mailer = UserAccountMailer()
        mailer.new_user_email(first_name, last_name, email)
    except Exception as ex:
        logger.error(ex, exc_info=True)
        raise ex


@flask_app_celery.task(bind=True, queue='login_otp')
def login_otp_email(unused_arg, otp, email_id, first_name=''):
    try:
        logger.info(f'Sending Login verification code {otp} to {email_id}')
        mailer = UserAccountMailer()
        mailer.send_login_otp(otp, email_id, first_name)
    except Exception as ex:
        logger.error(ex, exc_info=True)
        raise ex


@flask_app_celery.task(bind=True, queue='update_user_email')
def update_user_email(unused_arg, email_id, first_name):
    try:
        logger.info(f'Sending update successful email to {email_id}')
        mailer = UserAccountMailer()
        mailer.update_user_email(email_id, first_name)
    except Exception as ex:
        logger.error(ex, exc_info=True)
        raise ex


@flask_app_celery.task(bind=True, queue='update_user_email')
def reset_password_success_email(unused_arg, email_id, first_name):
    try:
        logger.info(f'Sending password reset successful email to {email_id}')
        mailer = PasswordMailer()
        mailer.reset_password_success_email(email_id, first_name)
    except Exception as ex:
        logger.error(ex, exc_info=True)
        raise ex


@flask_app_celery.task(bind=True, queue='password_reset')
def password_reset_email(unused_arg, email_id, password_reset_token):
    try:
        logger.info(f'Sending password reset email to {email_id} with verification code {password_reset_token}')
        mailer = PasswordMailer()
        mailer.reset_password_mail(email_id, password_reset_token)
    except Exception as ex:
        logger.error(ex, exc_info=True)
        raise ex
