from celery import Celery
from celery.schedules import crontab

from config import DevelopmentConfig

flask_app_celery = Celery(
    backend=DevelopmentConfig.CELERY_RESULT_BACKEND,
    broker=DevelopmentConfig.CELERY_BROKER_URL
)

# Refer below link to know about various crontab options.
# https://docs.celeryproject.org/en/stable/userguide/periodic-tasks.html#crontab-schedules
flask_app_celery.conf.beat_schedule = {
    'run-sample-beat-job-every-5-minute': {
        'task': 'app.tasks.scheduled_tasks.sample_celery_beat_job',
        'schedule': crontab()  # Every 30 minutes, 24*7
    },
    'run-sample-beat-job-everyday-at-8-30-am-UTC': {
        'task': 'app.tasks.scheduled_tasks.sample_celery_beat_job',
        'schedule': crontab(hour=8, minute=30)  # 8:30 am UTC
    }
}


def on_failure(self, exc, task_id, args, kwargs, einfo):
    print('{0!r} failed: {1!r}'.format(task_id, exc))


def on_success(self, retval, task_id, args, kwargs):
    print('{0!r} success: {1!r}'.format(task_id, retval))


