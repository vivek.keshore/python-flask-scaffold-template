from app.tasks.base_task import flask_app_celery
from app.tasks.user_tasks import new_user_email, login_otp_email, update_user_email
from app.tasks.user_tasks import reset_password_success_email, password_reset_email
from app.tasks.scheduled_tasks import sample_celery_beat_job
