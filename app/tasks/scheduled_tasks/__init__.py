from celery.utils.log import get_task_logger
from app.tasks.base_task import flask_app_celery

logger = get_task_logger(__name__)


@flask_app_celery.task(bind=True, queue='sample_beat_job')
def sample_celery_beat_job(unused_arg):
    try:
        logger.info(f'Executing sample celery beat job.')
    except Exception as ex:
        logger.error(ex, exc_info=True)
        raise ex
