from flask_marshmallow import Marshmallow

ma = Marshmallow()


class BaseSchema(ma.Schema):
    __abstract__ = True
