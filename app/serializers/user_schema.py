from marshmallow import fields

from app.repository import AccountRoleViewRepo
from app.serializers.base_schema import BaseSchema


class UserProfileSchema(BaseSchema):
    class Meta:
        fields = ('first_name', 'last_name', 'email', 'phone', 'street', 'city', 'state', 'country', 'zip')


class UserSchema(BaseSchema):
    user_type = fields.Method('get_user_role')

    class Meta:
        fields = ('email', 'user_type', 'id')

    @staticmethod
    def get_user_role(user):
        role = AccountRoleViewRepo.get_account_role(str(user.id))
        return role.role_name if role else None
