import logging
import logging.config
import logging.handlers
import time

from flasgger import Swagger
from flask import Flask
from flask_cors import CORS
from werkzeug.utils import import_string

from app.api import API_BLUEPRINTS
from app.cache import flask_app_cache
from app.mailers import mail
from app.models import DB, MIGRATE
from app.flask_app_logger import config as log_config
from app.serializers import ma
from app.tasks import flask_app_celery
from seed import SEED_CLI

swagger_template = {
    "info": {
        "title": "Flask App Scaffold",
        "description": "Flask App scaffold with alembic, cache, celery, swagger, auth, models, logger etc",
        "version": "0.1.1",
        "contact": {
            "name": "Vivek Keshore",
            "email": "vivek.keshore@gmail.com",
        }
    },
    "components": {
        "securitySchemes": {
            "bearerAuth": {
                "type": "http",
                "scheme": "bearer",
                "bearerFormat": "JWT"
            }
        }
    },
    "security": [
        {
            "bearerAuth": []
        }
    ]
}


def create_app(app_config):
    """
    Initialize the core application.
    """

    # Creating the app
    logging.config.dictConfig(log_config.LOGGING_CONF)
    logging.Formatter.converter = time.gmtime

    flask_app = Flask(__name__, template_folder='email_templates', instance_relative_config=True)
    cfg = import_string(app_config)()
    flask_app.config.from_object(cfg)

    DB.init_app(flask_app)
    MIGRATE.init_app(flask_app, DB)

    # Binding Redis
    flask_app_cache.init_app(flask_app.config.get('REDIS_CACHE_URL', ''))

    # Adding CLI
    flask_app.cli.add_command(SEED_CLI)

    # Configuring and initializing swagger
    flask_app.config['SWAGGER'] = {
        'title': 'Flask Scaffold App APIs',
        'openapi': '3.0.2',
        'uiversion': 3,
        "specs_route": "/swagger/"
    }
    unused_swagger = Swagger(flask_app, template=swagger_template)

    allowed_origins = [
        '*'
    ]
    unused_cors = CORS(flask_app, resources={
        r"/api/*": {
            "origins": allowed_origins
        }
    })

    ma.init_app(flask_app)
    mail.init_app(flask_app)

    for blueprint, kwargs in API_BLUEPRINTS:
        flask_app.register_blueprint(blueprint, **kwargs)

    # Background tasks
    flask_app_celery.config_from_object(flask_app.config)

    # Enable sqlalchemy logging
    if flask_app.config.get('ENV', 'dev') == 'dev':
        # set log level can be configure from env variable
        logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

    return flask_app
