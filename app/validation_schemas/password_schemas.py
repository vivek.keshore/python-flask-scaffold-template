import re
from app.lib.constants import OTP_CHARS_COUNT


def validate_password(field, value, error):
    regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{6,20}$"
    pattern = re.compile(regex)
    is_valid = re.search(pattern, value)

    if not is_valid:
        error(
            field,
            'Must contain at least 8 total characters, including at least one number, one uppercase letter, one lowercase letter and one special character.'
        )


REQUEST_RESET_PASSWORD_SCHEMA = {
    'email': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 255,
        'required': True,
        'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$',
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxLength': 'is too long, maximum 255 characters.',
                'regex': 'Email format is not valid.'
            }
        }
    }
}

RESET_PASSWORD_SCHEMA = {
    'password': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 20,
        'required': True,
        'check_with': validate_password,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxlength': 'is too long, maximum 20 characters.'
            }
        }
    },
    'confirm_password': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 20,
        'required': True,
        'check_with': validate_password,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxLength': 'is too long, maximum 20 characters.'
            }
        }
    },
    'otp': {
        'type': 'string',
        'minlength': OTP_CHARS_COUNT,
        'maxlength': OTP_CHARS_COUNT,
        'required': True,
        'meta': {
            'custom_message': {
                'minlength': f'Verification code must contain only {OTP_CHARS_COUNT} characters.',
                'maxLength': f'Verification code must contain only {OTP_CHARS_COUNT} characters.'
            }
        }
    }
}
