import re
from app.models.users.role import UserTypeEnum


ALLOWED_USERTYPES = [
    UserTypeEnum.individuals.value, UserTypeEnum.admin.value,
    UserTypeEnum.others.value, UserTypeEnum.default.value
]


def validate_password(field, value, error):
    regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{6,20}$"
    pattern = re.compile(regex)
    is_valid = re.search(pattern, value)

    if not is_valid:
        error(field, 'Must contain at-least one digit, one uppercase, one lowercase and one special character')


UPDATE_USER_SCHEMA = {
    'email': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 100,
        'required': False,
        'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$',
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxLength': 'is too long, maximum 100 characters.',
                'regex': 'Email format is not valid.'
            }
        }
    },
    'is_data_provider': {
        'type': 'boolean',
        'required': False
    },
    'password': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 20,
        'required': False,
        'check_with': validate_password,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxlength': 'is too long, maximum 20 characters.'
            }
        }
    },
    'new_password': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 20,
        'required': False,
        'check_with': validate_password,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxlength': 'is too long, maximum 20 characters.'
            }
        }
    },
    'confirm_password': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 20,
        'required': False,
        'check_with': validate_password,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxlength': 'is too long, maximum 20 characters.'
            }
        }
    },
    'first_name': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 30,
        'required': False,
        'regex': '^[a-zA-Z0-9 ]*$',
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 1 characters.',
                'maxLength': 'is too long, maximum 30 characters.',
                'regex': 'First name should contain alphanumeric characters only.'
            }
        }
    },
    'last_name': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 30,
        'required': False,
        'regex': '^[a-zA-Z0-9 ]*$',
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 1 characters.',
                'maxLength': 'is too long, maximum 30 characters.',
                'regex': 'Last name should contain alphanumeric characters only.'
            }
        }
    },
    'user_type': {
        'type': 'string',
        'allowed': ALLOWED_USERTYPES,
        'required': False,
        'meta': {
            'custom_message': {
                'allowed': f'User type is not valid. Valid user types are {", ".join(ALLOWED_USERTYPES)}'
            }
        }
    },
    'street': {
        'type': 'string',
        'minlength': 3,
        'maxlength': 50,
        'required': False,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 3 characters.',
                'maxLength': 'is too long, maximum 50 characters.'
            }
        }
    },
    'phone': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 15,
        'required': False,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxLength': 'is too long, maximum 15 characters.'
            }
        }
    },
    'city': {
        'type': 'string',
        'minlength': 2,
        'maxlength': 20,
        'required': False,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 2 characters.',
                'maxLength': 'is too long, maximum 20 characters.'
            }
        }
    },
    'state': {
        'type': 'string',
        'minlength': 3,
        'maxlength': 30,
        'required': False,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 3 characters.',
                'maxLength': 'is too long, maximum 30 characters.'
            }
        }
    },
    'country': {
        'type': 'string',
        'minlength': 3,
        'maxlength': 30,
        'required': False,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 3 characters.',
                'maxLength': 'is too long, maximum 30 characters.'
            }
        }
    },
    'zip': {
        'type': 'string',
        'minlength': 5,
        'maxlength': 8,
        'required': False,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 5 characters.',
                'maxLength': 'is too long, maximum 8 characters.'
            }
        }
    }
}
