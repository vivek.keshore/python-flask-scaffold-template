from app.lib.constants import OTP_CHARS_COUNT


LOGIN_SCHEMA = {
    'username': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 255,
        'required': True,
        'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$',
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxLength': 'is too long, maximum 255 characters.',
                'regex': 'Email format is not valid.'
            }
        }
    },
    'password': {
        'type': 'string',
        'minlength': 8,
        'maxlength': 20,
        'required': True,
        'meta': {
            'custom_message': {
                'minlength': 'is too short, minimum 8 characters.',
                'maxlength': 'is too long, maximum 20 characters.'
            }
        }
    }
}


OTP_SCHEMA = {
    'otp': {
        'type': 'string',
        'minlength': OTP_CHARS_COUNT,
        'maxlength': OTP_CHARS_COUNT,
        'required': True,
        'meta': {
            'custom_message': {
                'minlength': f'Verification code must contain only {OTP_CHARS_COUNT} characters.',
                'maxLength': f'Verification code must contain only {OTP_CHARS_COUNT} characters.'
            }
        }
    }
}