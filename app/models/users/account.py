from datetime import datetime

from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import Column, ForeignKey
from sqlalchemy import DateTime
from sqlalchemy import Index
from sqlalchemy import String, Boolean
from sqlalchemy import func as sa_func
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from app.lib.utils import us_format_phone
from app.models.base_model import BaseModel, UpdateMixin, AuditMixin


class Account(BaseModel, UpdateMixin):
    __tablename__ = 'account'
    __table_args__ = (
        Index('idx_account_email_unique', 'email', unique=True),
        {'schema': 'users'}
    )

    _email = Column('email', String(255), unique=True, nullable=False)
    _password_hash = Column('password_hash', String(255), nullable=False)
    is_active = Column(Boolean, default=True)
    last_login_at = Column(DateTime(timezone=True), default=datetime.utcnow)

    # Relationships
    profile = relationship("Profile", uselist=False, backref="account")
    roles = relationship("AccountRole", uselist=False, backref="account")

    @hybrid_property
    def email(self):
        return self._email

    @email.setter
    def email(self, email_id):
        self._email = email_id.lower()

    @hybrid_property
    def password_hash(self):
        return self._password_hash

    @password_hash.setter
    def password_hash(self, password):
        self._password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self._password_hash)


class Profile(BaseModel, AuditMixin):
    __tablename__ = 'profile'
    __table_args__ = (
        Index('idx_profile_email_unique', 'email', unique=True),
        {'schema': 'users'}
    )

    account_id = Column(UUID, ForeignKey(Account.id))
    _email = Column('email', String(100), unique=True)
    first_name = Column(String(128), nullable=False)
    last_name = Column(String(128), nullable=False)
    _phone = Column('phone', String(64), nullable=True)
    street = Column(String(128), nullable=True)
    city = Column(String(32), nullable=True)
    state = Column(String(30), nullable=True)
    country = Column(String(24), nullable=True)
    zip = Column(String(24), nullable=True)

    @hybrid_property
    def email(self):
        return self._email

    @email.setter
    def email(self, email_id):
        self._email = email_id.lower()

    @hybrid_property
    def phone(self):
        return self._phone

    @phone.setter
    def phone(self, phone_num):
        self._phone = us_format_phone(phone_num)
