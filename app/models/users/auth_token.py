from datetime import datetime

from sqlalchemy import Column, ForeignKey
from sqlalchemy import DateTime
from sqlalchemy import Index
from sqlalchemy import String, Boolean
from sqlalchemy.dialects.postgresql import UUID

from app.models.base_model import BaseModel
from app.models.users import Account


class AuthTokens(BaseModel):
    __tablename__ = 'auth_tokens'
    __table_args__ = (
        Index('idx_auth_tokens_token_hash', 'token_hash'),
        {'schema': 'users'}
    )

    user_id = Column(UUID, ForeignKey(Account.id))
    token = Column(String(280), nullable=False)
    token_hash = Column(String(256))
    aes_key = Column(String(128))
    aes_iv = Column(String(64))
    valid_till = Column(DateTime(timezone=True), nullable=False)
    is_active = Column(Boolean, default=True)
    ip_address = Column(String(50), nullable=False)
    created_on = Column(DateTime(timezone=True), default=datetime.utcnow)
