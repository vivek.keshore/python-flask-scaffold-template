import enum

from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Boolean
from sqlalchemy.dialects.postgresql import UUID, JSONB
from sqlalchemy.orm import relationship

from app.lib.constants import USER_TYPE_INDIVIDUALS, USER_TYPE_DEFAULT, USER_TYPE_OTHERS, USER_TYPE_ADMIN
from app.models.base_model import BaseModel, AuditMixin, DB
from app.models.enum_meta import EnumMeta
from app.models.users import Account


class UserTypeEnum(enum.Enum, metaclass=EnumMeta):
    """
    This enum defines the user type.
    """
    admin = USER_TYPE_ADMIN
    individuals = USER_TYPE_INDIVIDUALS
    others = USER_TYPE_OTHERS
    default = USER_TYPE_DEFAULT


class Roles(BaseModel, AuditMixin):
    __tablename__ = 'roles'
    __table_args__ = (
        {'schema': 'users'},
    )

    name = Column(String(255), unique=True, nullable=False)
    description = Column(String(255))
    display_name = Column(String(255))
    config = Column(JSONB)

    # Relationships
    account_roles = relationship("AccountRole", back_populates='roles')


class AccountRole(BaseModel, AuditMixin):
    __tablename__ = 'account_role'
    __table_args__ = (
        {'schema': 'users'},
    )

    is_approved = Column(Boolean, default=False)
    role_id = Column(UUID, ForeignKey(Roles.id))
    account_id = Column(UUID, ForeignKey(Account.id))

    roles = relationship("Roles", back_populates='account_roles')


class AccountRoleView(DB.Model):
    __tablename__ = 'account_role_view'
    __table_args__ = (
        {'schema': 'users'},
    )

    is_approved = Column(Boolean, primary_key=True)
    role_name = Column(String(255), primary_key=True)
    account_email = Column(String(255), primary_key=True)
    role_id = Column(UUID, primary_key=True)
    account_id = Column(UUID, primary_key=True)
