from datetime import datetime
from uuid import uuid4

from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, DateTime
from sqlalchemy.dialects.postgresql import UUID

DB = SQLAlchemy()
MIGRATE = Migrate()


class BaseModel(DB.Model):
    __abstract__ = True

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    def set_attributes(self, values):
        for key, value in values.items():
            if hasattr(self, key) and isinstance(value, str) and value:
                setattr(self, key, value)


class AuditMixin(DB.Model):
    __abstract__ = True

    created_on = Column(DateTime(timezone=True), default=datetime.utcnow)
    modified_on = Column(DateTime(timezone=True), default=datetime.utcnow, onupdate=datetime.utcnow)


class UpdateMixin(DB.Model):
    __abstract__ = True
    modified_on = Column(DateTime(timezone=True), default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)
    modified_by = Column(UUID(as_uuid=True))


class SubmittedMixin(DB.Model):
    __abstract__ = True
    submitted_on = Column(DateTime(timezone=True), default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)
    submitted_by = Column(UUID(as_uuid=True), nullable=False)


class ReviewedMixin(DB.Model):
    __abstract__ = True
    reviewed_on = Column(DateTime(timezone=True))
    reviewed_by = Column(UUID(as_uuid=True))
