import enum

from sqlalchemy import Column, String, Enum

from app.models.base_model import BaseModel, AuditMixin


class FileType(str, enum.Enum):  # To jsonify the enum we have to convert the enum to string object
    image = 'image'
    video = 'video'
    pdf = 'pdf'


class SampleTableWithEnumColumn(BaseModel, AuditMixin):
    __tablename__ = "sample_table_with_enum_column"
    __table_args__ = (
        {"schema": "experimental"},
    )

    name = Column(String(50))
    title = Column(String(250))
    description = Column(String(2048))
    media_type = Column(Enum(FileType), name='file_type')
