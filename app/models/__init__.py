""" Models """
from app.models.base_model import MIGRATE, DB
from app.models.users import Account, Profile, Roles, AccountRole, AuthTokens
from app.models.logs import ThreatLogs
from app.models.experimental import SampleTableWithEnumColumn


@MIGRATE.configure
def configure_alembic(config):
    # modify config object
    return config
