from datetime import datetime

from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Index
from sqlalchemy import String

from app.models.base_model import BaseModel


class ThreatLogs(BaseModel):
    __tablename__ = 'threat_logs'
    __table_args__ = (
        Index('idx_threat_logs_ip_address', 'ip_address'),
        {'schema': 'logs'}
    )

    ip_address = Column(String(50), nullable=False)
    email = Column(String(140), nullable=False)
    threat_type = Column(String(50))
    created_on = Column(DateTime(timezone=True), default=datetime.utcnow)
