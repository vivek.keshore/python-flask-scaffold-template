import enum


class EnumMeta(enum.EnumMeta):
    def __contains__(cls, item):
        return item in {v.value for v in cls.__members__.values()}

    def get_values(cls):
        return [member.value for member in cls.__members__.values()]
