from datetime import datetime, timedelta

from app.lib import Singleton
from app.lib.constants import IP_BLOCK_DURATION
from app.models.logs import ThreatLogs
from app.repository.sql_context import SqlContext


class ThreatRequestLogRepo(metaclass=Singleton):

    @staticmethod
    def add_new_threat_log(ip_address, email, threat_type):
        threat_log = ThreatLogs()
        threat_log.ip_address = ip_address
        threat_log.email = email
        threat_log.threat_type = threat_type

        with SqlContext() as sql_context:
            sql_context.session.add(threat_log)

        return threat_log

    @staticmethod
    def get_previous_suspected_threats(ip_address, threat_type='', email=''):
        ip_block_duartion = datetime.utcnow() - timedelta(hours=IP_BLOCK_DURATION)
        query = ThreatLogs.query.filter(
            ThreatLogs.ip_address == ip_address,
            ThreatLogs.created_on > ip_block_duartion
        )

        if threat_type:
            query = query.filter(ThreatLogs.threat_type == threat_type)

        if email:
            query = query.filter(ThreatLogs.email == email)

        return query.count()
