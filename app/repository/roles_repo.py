from datetime import datetime

from app.lib import Singleton
from app.repository.sql_context import SqlContext
from app.models.users.role import Roles, AccountRole


class RolesRepo(metaclass=Singleton):

    @staticmethod
    def get_role_by_name(name):
        query = Roles.query.filter(Roles.name == name)
        return query.scalar()


class AccountRoleRepo(metaclass=Singleton):
    @staticmethod
    def get_account_role(account_id):
        query = AccountRole.query.filter(AccountRole.account_id == str(account_id))
        return query.scalar()

    @staticmethod
    def get_role_by_account_id_role_id(account_id, role_id):
        query = AccountRole.query.filter(AccountRole.role_id == str(role_id), AccountRole.account_id == str(account_id))
        return query.scalar()

    @staticmethod
    def update_account_role_is_approved(account_role, is_approved, commit=False):
        account_role.is_approved = is_approved
        account_role.modified_on = datetime.utcnow()

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.merge(account_role)

        return account_role

    @staticmethod
    def create_update_account_role(account_id, role_name, is_approved=False, commit=False):
        role = RolesRepo.get_role_by_name(role_name)
        account_role = AccountRoleRepo.get_role_by_account_id_role_id(str(account_id), str(role.id))

        if not account_role:
            account_role = AccountRole()
            account_role.account_id = str(account_id)
            account_role.role_id = str(role.id)

        account_role.is_approved = is_approved

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.add(account_role)

        return account_role
