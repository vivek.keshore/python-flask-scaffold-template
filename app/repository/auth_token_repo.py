import hashlib
from datetime import datetime

from app.lib import Singleton
from app.lib.jwt_token import JwtToken
from app.models.users import AuthTokens
from app.repository.sql_context import SqlContext
from app.lib.cryptor import FlaskAppCryptor


class AuthTokenRepo(metaclass=Singleton):
    @staticmethod
    def get_auth_token_by_hash(token):
        auth_token = AuthTokens.query.filter_by(token_hash=token).scalar()
        return auth_token

    @staticmethod
    def invalidate_auth_token(token):
        auth_token = AuthTokens.query.filter_by(token_hash=token).one()
        auth_token.is_active = False
        with SqlContext() as sql_context:
            sql_context.session.merge(auth_token)

        return auth_token

    @staticmethod
    def add_new_auth_token(user_id, token, commit=True):
        payload = JwtToken.verify_token(token)
        jwt_token = token.decode()
        token_hash = hashlib.sha512(jwt_token.encode()).hexdigest()

        auth_token = AuthTokens()
        auth_token.user_id = str(user_id)
        auth_token.token = jwt_token
        auth_token.token_hash = token_hash
        auth_token.valid_till = datetime.utcfromtimestamp(payload['exp'])
        auth_token.aes_iv, auth_token.aes_key = FlaskAppCryptor.get_aes_key_iv_pair_random()
        auth_token.ip_address = payload['sub']

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.add(auth_token)

        return auth_token
