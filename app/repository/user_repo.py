from app.lib import Singleton
from app.lib.constants import USER_TYPE_DEFAULT, USER_TYPE_ADMIN
from app.lib.utils import utc_timestamp
from app.models.users import Account, Profile, AccountRoleView
from app.models.users import UserTypeEnum
from app.repository.sql_context import SqlContext


class UserRepo(metaclass=Singleton):
    @staticmethod
    def get_all_users():
        users = Account.query.all()
        return users

    @staticmethod
    def get_user_by_email(email):
        user = Account.query.filter(Account.email.ilike(email))
        return user.scalar()

    @staticmethod
    def get_user_by_id(user_id):
        user = Account.query.get(str(user_id))
        return user

    @staticmethod
    def add_new_user(email, password, commit=True):
        user = Account()
        user.email = email
        user.password_hash = password

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.add(user)

        return user

    @staticmethod
    def update_password(user, password):
        user.password_hash = password

        with SqlContext() as sql_context:
            sql_context.session.merge(user)

        return True

    @staticmethod
    def update_last_login(user):
        user.last_login_at = utc_timestamp()

        with SqlContext() as sql_context:
            sql_context.session.merge(user)

        return True

    @staticmethod
    def deactivate_user(user):
        user.is_active = False

        with SqlContext() as sql_context:
            sql_context.session.merge(user)

        return True

    @staticmethod
    def update_user(user, email='', new_password='', commit=False):
        is_user_updated = False
        is_password_updated = False

        if email:
            user.email = email
            is_user_updated = True

        if new_password:
            user.password_hash = new_password
            is_password_updated = True

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.merge(user)

        return user, is_user_updated, is_password_updated


class UserProfileRepo(metaclass=Singleton):
    @staticmethod
    def get_user_profile_by_email(email):
        user_profile = Profile.query.filter(Profile.email.ilike(email))
        return user_profile.scalar()

    @staticmethod
    def get_user_profile_by_id(id_):
        user_profile = Profile.query.get(str(id_))
        return user_profile

    @staticmethod
    def add_new_profile(first_name, last_name, email, phone, street, city, state, country, zip, account_id=None, commit=True):
        user_profile = Profile()

        if account_id:
            user_profile.account_id = account_id

        user_profile.first_name = first_name
        user_profile.last_name = last_name
        user_profile.email = email
        user_profile.phone = phone
        user_profile.street = street
        user_profile.city = city
        user_profile.state = state
        user_profile.country = country
        user_profile.zip = zip

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.add(user_profile)

        return user_profile

    @staticmethod
    def update_user_profile(user_profile, first_name='', last_name='', email='', phone='',
                            street='', city='', state='', country='', zipcode='', commit=False):

        params = dict((
            ('first_name', first_name), ('last_name', last_name), ('email', email), ('phone', phone),
            ('street', street), ('city', city), ('state', state), ('country', country), ('zip', zipcode))
        )

        is_profile_updated = False
        for attr, val in params.items():
            if val:
                setattr(user_profile, attr, val)
                is_profile_updated = True

        if commit:
            with SqlContext() as sql_context:
                sql_context.session.merge(user_profile)

        return user_profile, is_profile_updated


class AccountRoleViewRepo(metaclass=Singleton):
    @staticmethod
    def get_account_role(account_id):
        role = AccountRoleView.query.filter(
            AccountRoleView.account_id == str(account_id),
        )
        return role.scalar()

    @staticmethod
    def get_admin_account(account_id):
        query = AccountRoleView.query.filter(
            AccountRoleView.account_id == str(account_id),
            AccountRoleView.role_name == USER_TYPE_ADMIN
        )
        return query.scalar()

    @staticmethod
    def get_role_by_account_role_name(role_name, account_id):
        query = AccountRoleView.query.filter(
            AccountRoleView.account_id == str(account_id),
            AccountRoleView.role_name == role_name
        )
        return query.scalar()
