import hashlib

from flask import current_app as flask_app

from app.cache import flask_app_cache
from app.lib import Singleton
from app.lib.constants import AUTH_TOKEN_EXPIRATION
from app.lib.cryptor import FlaskAppCryptor
from app.lib.custom_exception import DBCreateRecordException
from app.lib.jwt_token import JwtToken
from app.repository import AuthTokenRepo, UserRepo


class TokenService(metaclass=Singleton):

    @staticmethod
    def create_temp_token(ip_address, expiration):
        flask_app.logger.info(f'Service called to create temp token. IP - {ip_address} | expiration - {expiration} seconds')

        jwt_token = JwtToken.create_jwt_token(ip_address, expiration)
        token_hash = hashlib.sha512(jwt_token).hexdigest()
        aes_iv, aes_key = FlaskAppCryptor.get_aes_key_iv_pair_random()

        cache_data = {
            'jwt_token': jwt_token.decode(),
            'ip_address': ip_address,
            'is_active': True,
            'aes_iv': aes_iv,
            'aes_key': aes_key
        }

        flask_app_cache.set(token_hash, cache_data, expiration=expiration)

        return token_hash, aes_key, aes_iv

    @staticmethod
    def create_auth_token(ip_address, user, expiration=AUTH_TOKEN_EXPIRATION):
        flask_app.logger.info(
            f'Service called to create auth token. IP - {ip_address} | expiration - {expiration} seconds | user_id - {str(user.id)}')
        auth_token = JwtToken.create_jwt_token(ip_address, expiration, user.id)
        try:
            auth_token_obj = AuthTokenRepo.add_new_auth_token(user.id, auth_token)
        except Exception as ex:
            error_msg = f'Error has occurred while creating auth token. user - {user.id} | IP - {ip_address}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')
            raise DBCreateRecordException('Error has occurred while creating auth token.')

        try:
            flask_app.logger.info(f'Updating last_login_at for user {user.id}')
            UserRepo.update_last_login(user)
        except Exception as ex:
            error_msg = f'Error has occurred while updating last login for user - {user.id}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')

        return auth_token_obj
