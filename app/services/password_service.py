from flask import current_app as flask_app
from flask import request

from app.cache import flask_app_cache
from app.lib import Singleton
from app.lib.constants import RESET_TOKEN_EXPIRATION_LIMIT, REQUEST_TYPE_RESET_PASSWORD, SUSPECTED_THREAT_COUNT_THRESHOLD
from app.lib.custom_exception import DBFetchFailureException, SuspectedMaliciousIPError, TokenExpiredException
from app.lib.custom_exception import DBRecordNotFound, DBCreateRecordException, JSONDataException, InvalidResetTokenException
from app.lib.utils import generate_otp
from app.repository.threat_log_repo import ThreatRequestLogRepo
from app.repository.user_repo import UserRepo


class PasswordService(metaclass=Singleton):
    @staticmethod
    def generate_reset_token(email):
        """
        Service method to generate a reset token and create new password history record.
    
        Args:
            email (str): Email address of user whose password needs to be reset.
    
        Returns:
            string: 6 char password reset token.
    
        Raises:
            DBFetchFailureException: Raised when fetching of user record fails.
            DBRecordNotFound: Raised when user record is not found for given email.
            DBCreateRecordException: Raised when the threat log record weren't created successfully.
        """
        flask_app.logger.info(f'Trying to generate new reset token for email - {email}')
        request_ip = request.headers.get('x-forwarded-for', '') or request.environ['REMOTE_ADDR']
        try:
            threat_count = ThreatRequestLogRepo.get_previous_suspected_threats(request_ip, REQUEST_TYPE_RESET_PASSWORD)
        except Exception as ex:
            error_msg = f'Unable to fetch previous threat logs for ip - {request_ip}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBFetchFailureException

        if threat_count >= SUSPECTED_THREAT_COUNT_THRESHOLD:
            raise SuspectedMaliciousIPError

        try:
            ThreatRequestLogRepo.add_new_threat_log(request_ip, email, REQUEST_TYPE_RESET_PASSWORD)
        except Exception as ex:
            error_msg = f'Unable to create suspected threat log record for ip - {request_ip}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBCreateRecordException

        try:
            user = UserRepo.get_user_by_email(email)
        except Exception as ex:
            error_msg = f'Unable to fetch user record for email address - {email}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBFetchFailureException

        if not user:
            error_msg = f'No valid user record found with email address - {email}'
            flask_app.logger.debug(error_msg)
            raise DBRecordNotFound

        flask_app.logger.info(f'Potential security threat. {email} has requested for reset password token.')
        try:
            password_reset_token = generate_otp()
            flask_app_cache.set(password_reset_token, email, expiration=RESET_TOKEN_EXPIRATION_LIMIT)
        except Exception as ex:
            error_msg = f'Unable to generate new reset token for email - {email}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBCreateRecordException

        return password_reset_token

    @staticmethod
    def validate_verification_code(otp):
        flask_app.logger.info(f'Trying to validate reset password verification code - {otp}')
        request_ip = request.headers.get('x-forwarded-for', '') or request.environ['REMOTE_ADDR']
        try:
            threat_count = ThreatRequestLogRepo.get_previous_suspected_threats(request_ip, REQUEST_TYPE_RESET_PASSWORD)
        except Exception as ex:
            error_msg = f'Unable to fetch previous threat logs for ip - {request_ip}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBFetchFailureException

        if threat_count >= SUSPECTED_THREAT_COUNT_THRESHOLD:
            raise SuspectedMaliciousIPError

        try:
            ThreatRequestLogRepo.add_new_threat_log(request_ip, otp, REQUEST_TYPE_RESET_PASSWORD)
        except Exception as ex:
            error_msg = f'Unable to create suspected threat log record for ip - {request_ip}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBCreateRecordException

        cached_obj = flask_app_cache.get(otp)
        return True if cached_obj else False

    @staticmethod
    def reset_password_with_token(reset_token, password, confirm_password):
        flask_app.logger.info(f'Trying to update the password with token - {reset_token}')

        if password != confirm_password:
            raise JSONDataException("Confirmation password doesn't match.")
        if not reset_token:
            raise InvalidResetTokenException('Reset Token is not provided.')

        email = flask_app_cache.get(reset_token)
        if not email:
            raise TokenExpiredException(f'{reset_token} is expired. Cannot reset the password.')

        try:
            user = UserRepo.get_user_by_email(email)
        except Exception as ex:
            error_msg = f'Unable to fetch user record with email {email}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBFetchFailureException

        try:
            UserRepo.update_password(user, password)
        except Exception as ex:
            error_msg = f'Unable to reset the password for email - {email}'
            flask_app.logger.error(f'{error_msg} - {ex}', exc_info=True)
            raise DBCreateRecordException

        flask_app_cache.delete(reset_token)
        return user
