from flask import g, current_app as flask_app

from app.api.auth import token_auth
from app.lib import Singleton
from app.lib.constants import USER_TYPE_DEFAULT, USER_TYPE_INDIVIDUALS
from app.lib.custom_exception import DBFetchFailureException, DBCreateRecordException, DBUpdateRecordException
from app.lib.custom_exception import DBRecordNotFound
from app.lib.custom_exception import DuplicateRecordException, JSONDataException, FailedUserCredentials
from app.models.users.role import UserTypeEnum
from app.repository import AccountRoleRepo
from app.repository import AuthTokenRepo
from app.repository import UserRepo, UserProfileRepo, AccountRoleViewRepo
from app.repository.sql_context import SqlContext


class UserService(metaclass=Singleton):

    @staticmethod
    def register_new_user(email, password, user_type, first_name, last_name,
                          phone, street, city, state, country, zip):
        flask_app.logger.info(f'Service called to create new user with email {email}')

        if user_type not in UserTypeEnum:
            flask_app.logger.error(f'User type {user_type} is invalid. User creation failed for user {email}')
            raise JSONDataException(f'User type is not valid - {user_type}')

        try:
            user = UserRepo.get_user_by_email(email)
            user_profile = UserProfileRepo.get_user_profile_by_email(email)
        except Exception as ex:
            error_msg = f'Error has occurred while checking if user already exist with email {email}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')
            raise DBFetchFailureException

        if user or user_profile:
            error_msg = f'User already exists with email {email}'
            flask_app.logger.error(error_msg)
            raise DuplicateRecordException(error_msg)

        try:
            user = UserRepo.add_new_user(email, password, commit=False)
            user_profile = UserProfileRepo.add_new_profile(
                first_name, last_name, email, phone, street,
                city, state, country, zip, commit=False
            )

            with SqlContext() as sql_context:
                sql_context.session.add(user)
                sql_context.session.flush()
                user_profile.account_id = str(user.id)
                user_role = AccountRoleRepo.create_update_account_role(
                    user.id, user_type, is_approved=True, commit=False
                )
                sql_context.session.add(user_profile)
                sql_context.session.add(user_role)

        except Exception as ex:
            error_msg = f'Error has occurred while creating user. {email}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')
            raise DBCreateRecordException

        flask_app.logger.info(f'New user created successfully with email {email}')
        return user

    @staticmethod
    def update_user(current_user, email='', old_password='', new_password='',
                    confirm_password='', first_name='', last_name='', user_type='',
                    phone='', street='', city='', state='', country='', zipcode=''):
        flask_app.logger.info(f'Service called to update user with email {current_user.email}')

        if user_type and user_type not in UserTypeEnum:
            flask_app.logger.error(f'Update user details failed. User type {user_type} is invalid. User update failed for user {current_user.email}')
            raise JSONDataException(f'User type is not valid - {user_type}')

        if new_password and confirm_password and old_password:
            if not current_user.verify_password(old_password):
                raise FailedUserCredentials('Current password is invalid.')

            if new_password == old_password:
                raise JSONDataException('New password cannot be same as old password.')

            if new_password != confirm_password:
                raise JSONDataException('New password and confirm password do not match.')

        is_email_changed = False
        if email and current_user.email != email:
            try:
                duplicate_profile = UserProfileRepo.get_user_profile_by_email(email)
            except Exception as ex:
                error_msg = f'Update user details failed. Error has occurred while fetching user profile with email {email}'
                flask_app.logger.error(f'{error_msg} | Error - {ex}')
                raise DBFetchFailureException

            if duplicate_profile:
                error_msg = f'Update user details failed. Another profile already exist with email {email}'
                flask_app.logger.error(error_msg)
                raise DuplicateRecordException(error_msg)

            is_email_changed = True

        try:
            user_profile = UserProfileRepo.get_user_profile_by_email(current_user.email)
        except Exception as ex:
            error_msg = f'Update user details failed. Error has occurred while fetching current user profile with email {current_user.email}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')
            raise DBFetchFailureException

        try:
            current_user, is_user_updated, is_password_updated = UserRepo.update_user(
                current_user, email, new_password, commit=False
            )
            user_profile, is_profile_updated = UserProfileRepo.update_user_profile(
                user_profile, first_name, last_name, email, phone,
                street, city, state, country, zipcode, commit=False
            )

            new_role = None
            old_account_role_detail = AccountRoleViewRepo.get_account_role(current_user.id)
            old_role = AccountRoleRepo.get_account_role(current_user.id)
            is_delete_old_role = True if user_type and old_account_role_detail.role_name != user_type else False
            if user_type or is_email_changed:
                if is_delete_old_role:
                    new_role = AccountRoleRepo.create_update_account_role(
                        current_user.id, user_type, commit=False
                    )
                else:
                    old_role.is_approved = False

            with SqlContext() as sql_context:
                sql_context.session.merge(current_user)
                sql_context.session.merge(user_profile)
                if is_delete_old_role:
                    sql_context.session.delete(old_role)
                    sql_context.session.add(new_role)
                else:
                    sql_context.session.merge(old_role)

        except Exception as ex:
            error_msg = f'Error has occurred while updating user. {current_user.email}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')
            raise DBUpdateRecordException

        flask_app.logger.info(f'User updated successfully. user id - {current_user.id}')
        return current_user, user_profile, is_user_updated or is_profile_updated, is_email_changed, is_password_updated

    @staticmethod
    def deactivate_user():
        flask_app.logger.info(f'Service called to deactivate the user {g.current_user.id}')

        try:
            UserRepo.deactivate_user(g.current_user)
        except Exception as ex:
            error_msg = f'Error has occurred while marking is_active as False for user {g.current_user.id}'
            flask_app.logger.error(f'{error_msg} | Error - {ex}')
            raise DBUpdateRecordException('Deactivating user record failed.')

        token = token_auth.get_auth()['token']
        AuthTokenRepo.invalidate_auth_token(token)
