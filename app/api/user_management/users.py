""" User management APIs """

from http import HTTPStatus

from flasgger import swag_from
from flask import Blueprint, jsonify, request, g
from flask import current_app as flask_app

from app.api.auth import authorize_user
from app.cache import flask_app_cache
from app.lib.constants import DEFAULT_ERROR_MESSAGE, SIGNUP_TOKEN_EXPIRATION
from app.lib.custom_exception import DBFetchFailureException, DBCreateRecordException, DBUpdateRecordException
from app.lib.custom_exception import DBRecordNotFound
from app.lib.custom_exception import DuplicateRecordException, JSONDataException, FailedUserCredentials
from app.lib.schema_validator import validate_json_schema
from app.serializers import UserSchema, UserProfileSchema
from app.services import UserService
from app.tasks.user_tasks import new_user_email, update_user_email
from app.tasks.user_tasks import reset_password_success_email
from app.validation_schemas.register_user_schema import REGISTER_USER_SCHEMA
from app.validation_schemas.update_user_schema import UPDATE_USER_SCHEMA

user_bp = Blueprint('user_apis', __name__)


@user_bp.route('/register', methods=['POST'])
@validate_json_schema(REGISTER_USER_SCHEMA)
@swag_from('/app/swagger_docs/users_and_auth/register_user.yml')
def register_user():
    email = request.json_data.get('email')
    password = request.json_data.get('password')
    first_name = request.json_data.get('first_name')
    last_name = request.json_data.get('last_name')
    user_type = request.json_data.get('user_type')
    phone = request.json_data.get('phone')
    street = request.json_data.get('street')
    city = request.json_data.get('city')
    state = request.json_data.get('state')
    country = request.json_data.get('country')
    zip = request.json_data.get('zip')

    try:

        signup_token = request.headers.get('Authorization', None)
        if signup_token is None:
            raise FailedUserCredentials(f'Register user Unsuccessful. No temp token found.')

        signup_token = signup_token.replace('Bearer ', '')
        temp_token = flask_app_cache.get(signup_token)
        if not temp_token or not temp_token.get('is_active'):
            raise FailedUserCredentials(f'Invalid signup temp token. token - {signup_token}')

        unused_user = UserService.register_new_user(
            email, password, user_type, first_name, last_name, phone, street, city, state, country, zip
        )

        temp_token['is_active'] = False
        flask_app_cache.set(signup_token, temp_token, expiration=SIGNUP_TOKEN_EXPIRATION)

    except FailedUserCredentials as ex:
        flask_app.logger.error(ex, exc_info=True)
        return {'status': 'error', 'message': f'Register user unsuccessful - {ex}.'}, HTTPStatus.UNAUTHORIZED

    except DBCreateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except DuplicateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.CONFLICT

    except DBFetchFailureException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except JSONDataException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.UNPROCESSABLE_ENTITY

    except DBRecordNotFound as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.UNPROCESSABLE_ENTITY

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR

    try:
        new_user_email.s(first_name, last_name, email).apply_async()
        flask_app.logger.info(f'New account creation email sent successfully to {email}')
    except Exception as e:
        flask_app.logger.error(f'New user registration email sending failed. {email}')

    return jsonify({
        'status': 'success',
        'message': f'Account created successfully for user {email}'
    }), HTTPStatus.CREATED


@user_bp.route('/details', methods=['GET'])
@authorize_user()
@swag_from('/app/swagger_docs/users_and_auth/get_user_details.yml')
def get_user_details():
    try:
        user_profile = g.current_user.profile
        user_dump = UserSchema().dump(g.current_user)
        user_dump.update(UserProfileSchema().dump(user_profile))

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'data': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR

    return jsonify({
        'status': 'success',
        'data': user_dump
    }), HTTPStatus.OK


@user_bp.route('/update', methods=['PUT'])
@authorize_user()
@validate_json_schema(UPDATE_USER_SCHEMA)
@swag_from('/app/swagger_docs/users_and_auth/update_user.yml')
def update_user():
    email = request.json_data.get('email')
    old_password = request.json_data.get('password')
    new_password = request.json_data.get('new_password')
    confirm_password = request.json_data.get('confirm_password')
    first_name = request.json_data.get('first_name')
    last_name = request.json_data.get('last_name')
    user_type = request.json_data.get('user_type')

    phone = request.json_data.get('phone')
    street = request.json_data.get('street')
    city = request.json_data.get('city')
    state = request.json_data.get('state')
    country = request.json_data.get('country')
    zipcode = request.json_data.get('zip')

    current_user = g.current_user
    previous_email = current_user.email
    try:
        updated_user, updated_profile, is_user_updated, is_email_changed, is_password_updated = UserService.update_user(
            current_user, email, old_password, new_password, confirm_password, first_name,
            last_name, user_type, phone, street, city, state, country, zipcode
        )

    except FailedUserCredentials as ex:
        flask_app.logger.error(ex, exc_info=True)
        return {'status': 'error', 'message': f'Update user unsuccessful - {ex}'}, HTTPStatus.FORBIDDEN

    except DBUpdateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except DuplicateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.CONFLICT

    except DBFetchFailureException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except JSONDataException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.UNPROCESSABLE_ENTITY

    except DBRecordNotFound as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.UNPROCESSABLE_ENTITY

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR

    # Sending email to user if user or profile details updated.
    if is_user_updated:
        try:
            update_user_email.s(updated_user.email, updated_profile.first_name).apply_async()
            flask_app.logger.info(f'Update user email sent successfully to {updated_user.email}')
        except Exception as e:
            flask_app.logger.error(f'Update user email sending failed. {updated_user.email}')

    # Sending email to old email id as well, in case email id changes.
    if is_email_changed:
        try:
            update_user_email.s(previous_email, updated_profile.first_name).apply_async()
            flask_app.logger.info(f'Update user email sent successfully to previous email {previous_email}')
        except Exception as e:
            flask_app.logger.error(f'Update user email sending failed to previous email. {previous_email}')

    # Sending email to inform user about password update.
    if is_password_updated:
        try:
            reset_password_success_email.s(updated_user.email, updated_profile.first_name).apply_async()
            flask_app.logger.info(f'Reset password success email sent successfully to {updated_user.email}')
        except Exception as e:
            flask_app.logger.error(f'Reset password success email sending failed. {updated_user.email}')

    return jsonify({
        'status': 'success',
        'message': f'Account updated successfully for user {current_user.email}'
    }), HTTPStatus.OK


@user_bp.route('/deactivate', methods=['DELETE'])
@authorize_user()
@swag_from('/app/swagger_docs/users_and_auth/deactivate_user.yml')
def deactivate_user():
    try:
        UserService.deactivate_user()

    except DBUpdateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': f'User deactivation unsuccessful. {DEFAULT_ERROR_MESSAGE}'}), HTTPStatus.INTERNAL_SERVER_ERROR

    response = {
        'status': 'success',
        'message': 'User deactivated'
    }
    return jsonify(response), HTTPStatus.NO_CONTENT
