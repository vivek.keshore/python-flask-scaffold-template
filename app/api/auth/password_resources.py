from http import HTTPStatus

from flasgger import swag_from
from flask import Blueprint, jsonify
from flask import current_app as flask_app
from flask import request

from app.lib.custom_exception import DBFetchFailureException, SuspectedMaliciousIPError
from app.lib.custom_exception import DBRecordNotFound, DBCreateRecordException, DEFAULT_ERROR_MESSAGE
from app.lib.custom_exception import InvalidResetTokenException, TokenExpiredException, JSONDataException
from app.lib.schema_validator import validate_json_schema
from app.services.password_service import PasswordService
from app.tasks.user_tasks import password_reset_email
from app.tasks.user_tasks import reset_password_success_email
from app.validation_schemas.login_schema import OTP_SCHEMA
from app.validation_schemas.password_schemas import REQUEST_RESET_PASSWORD_SCHEMA, RESET_PASSWORD_SCHEMA

password_reset_bp = Blueprint('Password resources', __name__)


@password_reset_bp.route('/password_reset', methods=['POST'])
@swag_from('/app/swagger_docs/users_and_auth/request_reset_password.yml')
@validate_json_schema(REQUEST_RESET_PASSWORD_SCHEMA)
def generate_reset_password_token():
    """
    API Method to generate and send new reset password token and instructions.
    """
    default_msg = 'You will receive an email with reset password instructions, if you are a registered user.'
    json_data = request.json
    email = json_data.get('email')
    if not email:
        raise Exception('Email id is not provided.')

    try:
        password_reset_token = PasswordService.generate_reset_token(email)
    except DBFetchFailureException as ex:
        return jsonify({'status': 'success', 'message': default_msg}), HTTPStatus.OK

    except DBRecordNotFound as ex:
        return jsonify({'status': 'success', 'message': default_msg}), HTTPStatus.OK

    except DBCreateRecordException as ex:
        return jsonify({'status': 'success', 'message': default_msg}), HTTPStatus.OK

    except SuspectedMaliciousIPError as ex:
        return jsonify({'status': 'error', 'message': 'Temporarily blocked due to too many attempts.'}), HTTPStatus.TOO_MANY_REQUESTS

    except Exception as ex:
        error_msg = f'An unexpected error has occurred while generating new reset token. Error - {ex}'
        flask_app.logger.error(error_msg)
        return jsonify({'status': 'success', 'message': default_msg}), HTTPStatus.OK

    try:
        password_reset_email.s(email, password_reset_token).apply_async()
    except Exception as ex:
        error_msg = f'An unexpected error has occurred while sending reset token email to {email}. Error - {ex}'
        flask_app.logger.error(error_msg, exc_info=True)
        return jsonify({'status': 'success', 'message': default_msg}), HTTPStatus.OK

    return jsonify({'status': 'success', 'message': default_msg}), HTTPStatus.OK


@password_reset_bp.route('/validate_verification_code', methods=['POST'])
@validate_json_schema(OTP_SCHEMA)
@swag_from('/app/swagger_docs/users_and_auth/validate_verification_code.yml')
def validate_verification_code():
    try:
        otp = request.json_data.get('otp')
        is_valid_otp = PasswordService.validate_verification_code(otp)
        if is_valid_otp:
            return jsonify({'status': 'success', 'message': 'Valid verification code.'}), HTTPStatus.OK
        else:
            return jsonify({'status': 'error', 'message': 'Invalid verification code.'}), HTTPStatus.UNAUTHORIZED

    except DBCreateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except DBFetchFailureException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except SuspectedMaliciousIPError as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error',
                        'message': 'Temporarily blocked due to too many attempts.'}), HTTPStatus.UNAUTHORIZED

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR


@password_reset_bp.route('/set_new_password', methods=['PUT'])
@swag_from('/app/swagger_docs/users_and_auth/reset_password.yml')
@validate_json_schema(RESET_PASSWORD_SCHEMA)
def set_new_password_using_token():
    """
    API method to set new password of user using reset token.
    """
    data = request.json
    password = data.get('password')
    confirm_password = data.get('confirm_password')
    reset_token = data.get('otp')

    try:
        user = PasswordService.reset_password_with_token(reset_token, password, confirm_password)

    except DBFetchFailureException as ex:
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except TokenExpiredException as ex:
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.BAD_REQUEST

    except JSONDataException as ex:
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.BAD_REQUEST

    except InvalidResetTokenException as ex:
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.BAD_REQUEST

    except Exception as ex:
        error_msg = f'An unexpected error has occurred while updating password. Error - {ex}'
        flask_app.logger.error(error_msg)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR

    try:
        reset_password_success_email.s(user.email, user.profile.first_name).apply_async()
        flask_app.logger.info(f'Reset password success email sent successfully to {user.email}')
    except Exception as e:
        flask_app.logger.error(f'Reset password success email sending failed. {user.email}')

    return jsonify({'status': 'success', 'message': 'Your password has been reset successfully!'}), HTTPStatus.OK
