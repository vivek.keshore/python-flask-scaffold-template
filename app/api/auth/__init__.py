from functools import wraps
from http import HTTPStatus

from flask import g, jsonify, make_response, request
from flask_httpauth import HTTPTokenAuth

from app.lib.constants import SUSPECTED_THREAT_COUNT_THRESHOLD
from app.lib.jwt_token import JwtToken
from app.repository import UserRepo, ThreatRequestLogRepo, AuthTokenRepo, AccountRoleViewRepo

token_auth = HTTPTokenAuth(scheme='Bearer')


@token_auth.error_handler
def auth_error(status):
    return jsonify(
        {
            'status': 'error',
            'message': 'Access Denied. Invalid auth token or IP is blocked. Unauthorized access to the Portal.'
        }
    ), status


@token_auth.verify_token
def verify_token(token):
    request_ip = request.headers.get('x-forwarded-for', '') or request.environ['REMOTE_ADDR']
    threat_count = ThreatRequestLogRepo.get_previous_suspected_threats(request_ip)
    if threat_count >= SUSPECTED_THREAT_COUNT_THRESHOLD:
        return False

    auth_token = AuthTokenRepo.get_auth_token_by_hash(token)
    if not auth_token or not auth_token.is_active:
        return False

    jwt_payload = JwtToken.verify_token(auth_token.token, request_ip)
    try:
        user = UserRepo.get_user_by_id(jwt_payload['user_id'])
        if not user.is_active or str(auth_token.user_id) != str(user.id):
            return False

        user.auth_token = auth_token
        g.current_user = user
        return True
    except Exception as ex:
        return False


def authorize_user(restrict_non_active_user=True):
    def decorated(api_method):
        @wraps(api_method)
        @token_auth.login_required
        def wrapper(*args, **kwargs):
            if not g.current_user.is_active and restrict_non_active_user:
                return make_response(jsonify(
                    'Access restricted as your account is not active.'), HTTPStatus.FORBIDDEN)

            return api_method(*args, **kwargs)

        return wrapper

    return decorated


def authorize_admin(restrict_non_active_user=True):
    def decorated(api_method):
        @wraps(api_method)
        @token_auth.login_required
        def wrapper(*args, **kwargs):
            if not g.current_user.is_active and restrict_non_active_user:
                return make_response(jsonify(
                    'Access restricted as your account is not active.'), HTTPStatus.FORBIDDEN)

            admin = AccountRoleViewRepo.get_admin_account(g.current_user.id)
            if not admin:
                return make_response(jsonify(
                    'Access restricted because you are not admin user.'), HTTPStatus.FORBIDDEN)

            if admin and not admin.is_approved:
                return make_response(jsonify(
                    'Access restricted because your admin role is not yet approved.'), HTTPStatus.FORBIDDEN)

            return api_method(*args, **kwargs)

        return wrapper

    return decorated


def authorize_role(role_name, restrict_non_active_user=True):
    def decorated(api_method):
        @wraps(api_method)
        @token_auth.login_required
        def wrapper(*args, **kwargs):
            if not g.current_user.is_active and restrict_non_active_user:
                return make_response(jsonify(
                    'Access restricted as your account is not active.'), HTTPStatus.FORBIDDEN)

            role = AccountRoleViewRepo.get_role_by_account_role_name(role_name, g.current_user.id)
            if not role:
                return make_response(jsonify(
                    f'Access restricted because you are not a {role_name}.'), HTTPStatus.FORBIDDEN)

            if role and not role.is_approved:
                return make_response(jsonify(
                    f'Access restricted because your {role_name} role is not yet approved.'), HTTPStatus.FORBIDDEN)

            return api_method(*args, **kwargs)

        return wrapper

    return decorated
