from http import HTTPStatus

from flasgger import swag_from
from flask import Blueprint, request, jsonify, g
from flask import current_app as flask_app

from app.api.auth import authorize_user, token_auth
from app.cache import flask_app_cache
from app.lib.constants import DEFAULT_ERROR_MESSAGE
from app.lib.constants import SUSPECTED_THREAT_COUNT_THRESHOLD, REQUEST_TYPE_LOGIN, OTP_VALIDITY
from app.lib.custom_exception import FailedUserCredentials, BlockedIPException, DBCreateRecordException
from app.lib.jwt_token import JwtToken
from app.lib.schema_validator import validate_json_schema
from app.lib.utils import generate_otp
from app.repository import AuthTokenRepo
from app.repository.threat_log_repo import ThreatRequestLogRepo
from app.repository.user_repo import UserRepo
from app.services.token_service import TokenService
from app.tasks.user_tasks import login_otp_email
from app.validation_schemas.login_schema import LOGIN_SCHEMA, OTP_SCHEMA

auth_bp = Blueprint('authentication', __name__)


@auth_bp.route('/login', methods=['POST'])
@swag_from('/app/swagger_docs/users_and_auth/login.yml')
@validate_json_schema(LOGIN_SCHEMA)
def login():
    """ Validate user credentials, if valid, return token else respective error message """
    try:
        request_ip = request.headers.get('x-forwarded-for', '') or request.environ['REMOTE_ADDR']
        login_token = request.headers.get('Authorization', None)
        if login_token is None:
            raise FailedUserCredentials(f'Login Unsuccessful. No authorization token found. ip - {request_ip}')

        login_token = login_token.replace('Bearer ', '')
        temp_token = flask_app_cache.get(login_token)
        if not temp_token or not temp_token.get('is_active'):
            raise FailedUserCredentials(f'Invalid login token. IP - {request_ip} | token - {login_token}')

        jwt_payload = JwtToken.verify_token(temp_token.get('jwt_token'))
        if not jwt_payload:
            raise FailedUserCredentials(f'Invalid login token. No JWT payload extracted. IP - {request_ip} | token - {login_token}')

        if request_ip != temp_token.get('ip_address'):
            raise FailedUserCredentials(f'Invalid login token. Request IP {request_ip} and IP linked to temp token {temp_token.get("ip_address")} are not matching.')

        credentials = request.json_data

        email, password = credentials['username'], credentials['password']
        user = UserRepo.get_user_by_email(email)
        if user is None:
            raise FailedUserCredentials(f'Login Unsuccessful. No user found with email {email}')

        threat_count = ThreatRequestLogRepo.get_previous_suspected_threats(request_ip, REQUEST_TYPE_LOGIN)
        if threat_count >= SUSPECTED_THREAT_COUNT_THRESHOLD:
            raise BlockedIPException(f'IP {request_ip} is blocked temporarily. Email - {email}')

        if not user.is_active:
            raise FailedUserCredentials(
                f'{email} is marked as Inactive. Please contact admin or application team.')

        if not user.verify_password(password):
            ThreatRequestLogRepo.add_new_threat_log(request_ip, email, REQUEST_TYPE_LOGIN)
            raise FailedUserCredentials(f'Login unsuccessful. User {email} has entered invalid password.')

        try:
            first_name = user.profile.first_name
        except Exception as ex:
            flask_app.logger.error(f'Unable to fetch full name from profile for user {user.email}', exc_info=True)
            first_name = user.email

        otp = generate_otp()
        login_otp_email.s(otp, email, first_name).apply_async()
        temp_token['otp'] = otp
        temp_token['user_id'] = str(user.id)
        flask_app_cache.set(login_token, temp_token, expiration=OTP_VALIDITY)

        return jsonify({'status': 'success',
                        'message': 'Multi-factor authentication sent to registered email id.'}), HTTPStatus.OK

    except FailedUserCredentials as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Login unsuccessful - Invalid credentials or token.'}), HTTPStatus.UNAUTHORIZED
    except BlockedIPException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Temporarily blocked due to too many attempts.'}), HTTPStatus.UNAUTHORIZED
    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR


@auth_bp.route('/validate_login_otp', methods=['POST'])
@validate_json_schema(OTP_SCHEMA)
@swag_from('/app/swagger_docs/users_and_auth/validate_login_otp.yml')
def validate_login_otp():
    try:
        request_ip = request.headers.get('x-forwarded-for', '') or request.environ['REMOTE_ADDR']

        threat_count = ThreatRequestLogRepo.get_previous_suspected_threats(request_ip, REQUEST_TYPE_LOGIN)
        if threat_count >= SUSPECTED_THREAT_COUNT_THRESHOLD:
            raise BlockedIPException(f'IP {request_ip} is blocked temporarily.')

        login_token = request.headers.get('Authorization', None)
        if login_token is None:
            raise FailedUserCredentials(f'Login Unsuccessful. No authorization token found. ip - {request_ip}')

        login_token = login_token.replace('Bearer ', '')
        temp_token = flask_app_cache.get(login_token)
        if not temp_token:
            raise FailedUserCredentials(f'Invalid login token. IP - {request_ip} | token - {login_token}')

        if not temp_token.get('is_active'):
            raise FailedUserCredentials(f'Invalid verification code or token. Verification code or token expired.')

        if request_ip != temp_token.get('ip_address'):
            raise FailedUserCredentials(f'Invalid login token. Request IP {request_ip} and IP linked to temp token {temp_token.get("ip_address")} are not matching.')

        otp = request.json_data.get('otp')

        if otp != temp_token['otp']:
            raise FailedUserCredentials(f'Invalid verification code. IP - {request_ip} | token - {login_token} | verification code - {otp}')

        user = UserRepo.get_user_by_id(temp_token['user_id'])
        if user is None:
            raise FailedUserCredentials(f'Login Unsuccessful. No user found with user id {temp_token["user_id"]}')

        auth_token = TokenService.create_auth_token(request_ip, user)
        res = {
            'token': auth_token.token_hash,
            'aes_key': auth_token.aes_key,
            'aes_iv': auth_token.aes_iv
        }

        temp_token['is_active'] = False
        flask_app_cache.set(login_token, temp_token, expiration=OTP_VALIDITY)

        return jsonify(res), HTTPStatus.OK

    except DBCreateRecordException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': str(ex)}), HTTPStatus.INTERNAL_SERVER_ERROR

    except FailedUserCredentials as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Login unsuccessful - Invalid verification code or token.'}), HTTPStatus.UNAUTHORIZED

    except BlockedIPException as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Temporarily blocked due to too many attempts.'}), HTTPStatus.UNAUTHORIZED

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR


@auth_bp.route('/logout', methods=['DELETE'])
@authorize_user()
@swag_from('/app/swagger_docs/users_and_auth/logout.yml')
def logout_user():
    try:
        flask_app.logger.info(f'Logging out the user {g.current_user.email}')
        token = token_auth.get_auth()['token']
        AuthTokenRepo.invalidate_auth_token(token)

    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': f'Logout unsuccessful. {DEFAULT_ERROR_MESSAGE}'}), HTTPStatus.INTERNAL_SERVER_ERROR

    response = {
        'status': 'success',
        'message': 'Logged out successfully'
    }
    return jsonify(response), HTTPStatus.OK
