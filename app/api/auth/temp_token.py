from http import HTTPStatus

from flask import Blueprint, request
from flask import current_app as flask_app
from flask import jsonify

from app.lib.constants import DEFAULT_ERROR_MESSAGE, LOGIN_TOKEN_EXPIRATION, SIGNUP_TOKEN_EXPIRATION
from app.services import TokenService
from flasgger import swag_from

temp_token_bp = Blueprint('temp_token', __name__)


def create_temp_token(expiration):
    ip_address = request.headers.get('x-forwarded-for', '') or request.environ['REMOTE_ADDR']
    if not ip_address:
        response = {
            'status': 'failure',
            'message': 'IP Address not available'
        }
        return jsonify(response), HTTPStatus.BAD_REQUEST

    try:
        token_hash, aes_key, aes_iv = TokenService.create_temp_token(ip_address, expiration)
    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify({'status': 'error', 'message': DEFAULT_ERROR_MESSAGE}), HTTPStatus.INTERNAL_SERVER_ERROR

    response = {
        'temp_token': token_hash,
        'aes_key': aes_key,
        'aes_iv': aes_iv
    }
    return jsonify(response), HTTPStatus.OK


@temp_token_bp.route('/login_token', methods=['GET'])
@swag_from('/app/swagger_docs/users_and_auth/login_token.yml')
def get_login_token():
    return create_temp_token(LOGIN_TOKEN_EXPIRATION)


@temp_token_bp.route('/signup_token', methods=['GET'])
@swag_from('/app/swagger_docs/users_and_auth/signup_token.yml')
def get_signup_token():
    return create_temp_token(SIGNUP_TOKEN_EXPIRATION)
