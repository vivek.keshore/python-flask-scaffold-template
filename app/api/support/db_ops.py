from http import HTTPStatus
from subprocess import Popen, PIPE

from flask import Blueprint, request
from flask import current_app as flask_app
from flask import jsonify

from app.api.auth import authorize_admin

support_bp = Blueprint('App Support', __name__)


@support_bp.route('/downgrade_db', methods=['GET'])
@authorize_admin()
def downgrade_db():
    try:
        process = Popen(['flask', 'db', 'downgrade'], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify('DB downgrade failed'), HTTPStatus.INTERNAL_SERVER_ERROR

    return jsonify(f"{stderr.decode('utf-8')} | {stdout.decode('utf-8')}"), HTTPStatus.OK


@support_bp.route('/seed_db', methods=['POST'])
@authorize_admin()
def seed_db():
    try:
        table = request.json_data.get('table', 'all')
        process = Popen(['flask', 'seed', table], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
    except Exception as ex:
        flask_app.logger.error(ex, exc_info=True)
        return jsonify('DB seed failed'), HTTPStatus.INTERNAL_SERVER_ERROR

    return jsonify(f"{stderr.decode('utf-8')} | {stdout.decode('utf-8')}"), HTTPStatus.OK
