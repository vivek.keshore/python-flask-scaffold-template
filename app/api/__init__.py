from flask import Blueprint

from app.api.auth.temp_token import temp_token_bp
from app.api.user_management.users import user_bp
from app.api.auth.authentication import auth_bp
from app.api.auth.password_resources import password_reset_bp
from app.api.support.db_ops import support_bp
from flasgger import swag_from
from app.api.auth import authorize_user

ping_bp = Blueprint('api', __name__)


@ping_bp.route('/ping')
@swag_from('/app/swagger_docs/ping.yml')
def health_check():
    return 'pong ... !'


API_BLUEPRINTS = [
    (user_bp, {'url_prefix': '/api/user'}),
    (temp_token_bp, {'url_prefix': '/api'}),
    (auth_bp, {'url_prefix': '/api'}),
    (support_bp, {'url_prefix': '/api'}),
    (password_reset_bp, {'url_prefix': '/api/user'}),
    (ping_bp, {'url_prefix': ''})
]
