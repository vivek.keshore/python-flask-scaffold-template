import string


DEFAULT_ERROR_MESSAGE = 'An error has occurred while processing your request.'

# User Types
USER_TYPE_INDIVIDUALS = 'Individuals'
USER_TYPE_OTHERS = 'Others'
USER_TYPE_DEFAULT = 'Default'
USER_TYPE_ADMIN = 'Admin'

# Default expiration limit for login token / OTP
LOGIN_TOKEN_EXPIRATION = 120  # Seconds
SIGNUP_TOKEN_EXPIRATION = 300  # Seconds
AUTH_TOKEN_EXPIRATION = 7200  # Seconds i.e. 2 hours
OTP_VALIDITY = 300  # Seconds
RESET_TOKEN_EXPIRATION_LIMIT = 3600  # seconds i.e. 1 hour

# Threat threshold, to block the IP after n attempts.
SUSPECTED_THREAT_COUNT_THRESHOLD = 5

REQUEST_TYPE_LOGIN = 'User Login'
REQUEST_TYPE_RESET_PASSWORD = 'Reset Password'

IP_BLOCK_DURATION = 2  # in hours

# Valid otp characters
VALID_OTP_CHARS = string.ascii_uppercase + string.digits
OTP_CHARS_COUNT = 6

# S3 file download link expiration limit
S3_LINK_EXPIRATION_LIMIT = 7200  # Seconds i.e. 2 hours
S3_TEMPLATE_FOLDER = 'download'
S3_SUBMITTED_DATA_FOLDER = 'upload'
