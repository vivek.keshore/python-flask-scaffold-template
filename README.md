## This project is developed by Vivek Keshore, SenecaGlobal, Inc.

## Sample flask app backend APIs

> after cloning the source code
### Setup

##### Python 3.8 and Redis

```commandline
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt install python3.8
$ sudo apt-get install redis-server
```

##### Pip3
```commandline
$ sudo apt install python3-pip
```

##### Pipenv
```commandline
$ pip3 install pipenv
```
> _assuming following commands are executing in project, after clone from gitlab._

##### Creating environment file
Update the .env file with respective values. Add environment variables in this file.
Important to put values for database config. 
Re-enable the virtual environment after adding the env variables in .env file.
```commandline
$ cp sample.env .env
```

##### Create virtual env and Install Packages

The first time it will check in `~/.virtualenvs` virtual environments folder whether a virtual environment is present for this application, 
if not pipenv will automatically create a new virtual environment

```commandline
$ pipenv install
```

for development dependencies
```commandline
$ pipenv install --dev
```

##### Enable virtual environment

Enabling the virtual env.

```commandline
$ pipenv shell
```

##### Add dependencies
> _If there is a need to add additional dependency in Pipfile file, then add it to pipfle and Run `pipenv install`. This will update pipfile.lock. While committing the code commit both *Pipfile* and *Pipfile.lock* files_
```.toml
[packages]
flask = "==1.1.2"
flask-mail = "==0.9.1"
Flask-Migrate = "==2.5.3"
Flask-SQLAlchemy = "==2.4.3"
Package-Name = "==version"
```

##### Updating database schema

Use [Flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/) if there is any add/delete/update in database models.
> _Run following command to run db uprade, and create tables in database. Assuming all db related environment variables are set in .env file and don't forget to create an empty database for the first time before running below command._ 
```commandline
$ flask db upgrade
```

##### Seeding database with sample data. (optional)
A custom flask command is added to this application. The custom flask command is seed.
You can call seed with any table name or schema name or "all".

To seed all the tables in all schemas
```commandline
$ flask seed all
```
To seed all the tables of a particular schema
```commandline
$ flask seed users
```
To a particular table
```commandline
$ flask seed account
$ flask seed profile
$ flask seed roles
```


##### Start Server
```commandline
$ flask run
```

Start server with gunicorn
```commandline
$ gunicorn run:flask_app -b 0.0.0.0:5000 --reload
```

##### Using celery for background tasks
> Celery command is put inside celery_cmd.sh. So, just execute the shell file as mentioned below. Alternatively, copy paste the celery command from shell file, and run directly in terminal.
```commandline
$ . celery_cmd.sh
```


##### Run docker container
> Run the docker container to run celery, celerybeat and backend server.
```commandline
$ sudo docker-compose up -d --build
```

> Take down docker container.
```commandline
$ sudo docker-compose down
```

