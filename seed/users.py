from seed.base_seeder import BaseSeeder


class LoadUsersSchema(BaseSeeder):
    @staticmethod
    def load_roles():
        LoadUsersSchema.create_records('customer', 'roles')

    @staticmethod
    def load_accounts():
        LoadUsersSchema.create_records('customer', 'account')

    @staticmethod
    def load_profile():
        LoadUsersSchema.create_records('customer', 'profile')

    @staticmethod
    def load_account_role():
        LoadUsersSchema.create_records('customer', 'account_role')

    @classmethod
    def all_tables(cls):
        cls.load_roles()
        cls.load_accounts()
        cls.load_profile()
        cls.load_account_role()
