import json
import os
import click
import csv
from json import JSONDecodeError

from app.models.users import Account, AccountRole, Profile, Roles
from app.repository.sql_context import SqlContext

table_name_map = {
    'account': Account,
    'account_role': AccountRole,
    'profile': Profile,
    'roles': Roles,
}


class BaseSeeder:
    @staticmethod
    def set_attributes(obj, values):
        for key, value in values.items():
            if hasattr(obj, key) and value:
                try:
                    loaded_value = json.loads(value)
                except JSONDecodeError:
                    pass
                else:
                    if isinstance(loaded_value, (list, dict, set)):
                        value = loaded_value

                if isinstance(value, str):
                    if value.lower() == 'true':
                        value = True
                    elif value.lower() == 'false':
                        value = False
                    elif value == 'NULL':
                        value = None

                setattr(obj, key, value)
        return obj

    @staticmethod
    def save_obj(obj, values):
        obj = BaseSeeder.set_attributes(obj, values)
        with SqlContext() as sql_context:
            sql_context.session.add(obj)

    @staticmethod
    def create_records(schema, table_name, delimiter=','):
        csv_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), f'metadata/{table_name}.csv')
        click.secho(f'\nseeding data from {csv_file} into {schema}.{table_name} table.', fg='yellow', bold=True)
        with open(csv_file) as data:
            rows = csv.DictReader(data, delimiter=delimiter)
            for row in rows:
                Table = table_name_map[table_name]
                obj = Table()
                try:
                    BaseSeeder.save_obj(obj, row)
                except Exception as ex:
                    click.secho(f'Table - {table_name} | row - {row} | Error - {ex}',
                                fg='red', bold=True)
        click.secho(f'seeding data from {csv_file} into {schema}.{table_name} table - Completed', fg='green', bold=True)
