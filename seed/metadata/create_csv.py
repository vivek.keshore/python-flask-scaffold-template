import random
from uuid import uuid4

from faker import Faker

from app.models.users.role import UserTypeEnum

USERS = 50
ROLES = UserTypeEnum.get_values()
domain = 'myflaskapp.com'
default_password = 'Vivek123$'

fake = Faker()


def create_csv_data():
    account_rows = []
    account_ids = []
    profile_rows = []

    for i in range(USERS):
        id = str(uuid4())
        account_id = str(uuid4())
        first_name = fake.first_name()
        last_name = fake.last_name()
        email = f'{first_name}.{last_name}@{domain}'
        phone = fake.phone_number()
        street = fake.street_address()
        city = fake.city()
        country = 'USA'
        state = fake.state()
        zip = fake.postcode()

        profile = [
            id, account_id, email, first_name, last_name, phone,
            street, city, state, country, zip
        ]

        account = [account_id, email, default_password]

        profile_rows.append(profile)
        account_rows.append(account)
        account_ids.append(account_id)

    return account_rows, profile_rows, account_ids


def write_account_profiles_csv(accounts, profiles):
    account_headers = [
        'id', 'email', 'password_hash'
    ]

    profile_headers = [
        'id', 'account_id', 'email', 'first_name', 'last_name', 'phone',
        'street', 'city', 'state', 'country', 'zip'
    ]
    with open('account.csv', 'w') as acc_csv:
        acc_csv.write(f'{",".join(account_headers)}\n')
        for account in accounts:
            acc_csv.write(f'{",".join(account)}\n')

    with open('profile.csv', 'w') as profile_csv:
        profile_csv.write(f'{",".join(profile_headers)}\n')
        for profile in profiles:
            profile_csv.write(f'{",".join(profile)}\n')


def write_roles_csv():
    roles_row = []
    role_ids = []
    for role in ROLES:
        id = str(uuid4())
        name = role
        description = f'{role} Role'
        display_name = name

        row = [id, name, description, display_name]
        roles_row.append(row)
        role_ids.append(id)

    header = ['id', 'name', 'description', 'display_name']
    with open('roles.csv', 'w') as roles_csv:
        roles_csv.write(f'{",".join(header)}\n')
        for row in roles_row:
            roles_csv.write(f'{",".join(row)}\n')

    return role_ids


def write_account_role(account_ids, role_ids):
    header = ['role_id', 'account_id']
    with open('account_role.csv', 'w') as acc_role:
        acc_role.write(f'{",".join(header)}\n')
        for account_id in account_ids:
            acc_role.write(f'{random.choice(role_ids)},{account_id}\n')


if __name__ == '__main__':
    accounts, profiles, account_ids = create_csv_data()
    write_account_profiles_csv(accounts, profiles)
    role_ids = write_roles_csv()
    write_account_role(account_ids, role_ids)
