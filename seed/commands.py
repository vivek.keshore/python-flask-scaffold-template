""" Seed data into database """
from flask.cli import AppGroup
from seed.users import LoadUsersSchema

SEED_CLI = AppGroup('seed', help='Seed initial data')


@SEED_CLI.command('roles')
def cmd_load_roles():
    LoadUsersSchema.load_roles()


@SEED_CLI.command('account')
def cmd_load_account():
    LoadUsersSchema.load_accounts()


@SEED_CLI.command('profile')
def cmd_load_profile():
    LoadUsersSchema.load_profile()


@SEED_CLI.command('account_role')
def cmd_load_account_role():
    LoadUsersSchema.load_account_role()


@SEED_CLI.command('users_schema')
def cmd_load_customer_schema():
    LoadUsersSchema.all_tables()


@SEED_CLI.command('all')
def cmd_load_all():
    LoadUsersSchema.all_tables()
