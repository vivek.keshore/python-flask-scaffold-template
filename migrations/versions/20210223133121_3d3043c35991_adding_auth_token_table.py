"""adding_auth_token_table

Revision ID: 3d3043c35991
Revises: 50ce0d9b4729
Create Date: 2021-02-23 13:31:21.398774

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '3d3043c35991'
down_revision = '50ce0d9b4729'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('auth_tokens',
    sa.Column('id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('user_id', postgresql.UUID(), nullable=True),
    sa.Column('token', sa.String(length=280), nullable=False),
    sa.Column('token_hash', sa.String(length=256), nullable=True),
    sa.Column('aes_key', sa.String(length=128), nullable=True),
    sa.Column('aes_iv', sa.String(length=64), nullable=True),
    sa.Column('valid_till', sa.DateTime(timezone=True), nullable=False),
    sa.Column('is_active', sa.Boolean(), nullable=True),
    sa.Column('ip_address', sa.String(length=50), nullable=False),
    sa.Column('created_on', sa.DateTime(timezone=True), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.account.id'], ),
    sa.PrimaryKeyConstraint('id'),
    schema='users'
    )
    op.create_index('idx_auth_tokens_token_hash', 'auth_tokens', ['token_hash'], unique=False, schema='users')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('idx_auth_tokens_token_hash', table_name='auth_tokens', schema='users')
    op.drop_table('auth_tokens', schema='users')
    # ### end Alembic commands ###
